import csv
import json

def generate_test_json():
	csvfile = open('test-cars.csv', 'r')
	jsonfile = open('test-cars.json', 'w')

	reader = csv.DictReader( csvfile )
	cars = []
	for row in reader:
		cars.append(row)

	json.dump(cars, jsonfile)

def crawl():
	pass

if __name__ == '__main__':
	