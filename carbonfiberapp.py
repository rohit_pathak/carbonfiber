from flask import Flask, render_template, jsonify, request
import json
import models
from bson import json_util


app = Flask(__name__)

# URL handlers
@app.route("/")
def index():
	makes = models.get_makes()
	return render_template( "results.html" )

@app.route("/search/", methods=['GET', 'POST'])
def search():
	if request.method == 'POST':
		data = []
		params = json.loads(request.data)['params']
		# print params
		keywords = params['keywords'].split()
		budget = ( int(params['budget']['min']), int(params['budget']['max']) )
		if budget[1] == 200000: budget = (budget[0], 100000000)

		makes = [m for m in params['makes'] if m['is_selected']]
		mileage = float(params['mileage'])
		body_types = [ k for k in params['bodyTypes'] if params['bodyTypes'][k] ]
		page = params['nextPage']
		
		data = models.get_models( keywords, budget, makes, mileage, body_types, page )

		if data.count(True) > 0: page += 1
		else: page = (-1)
		
		return json_util.dumps({ 'cars':data, 'nextPage':page, 'searchCount':data.count() })

	elif request.method == 'GET':
		return render_template('results.html')


@app.route("/about/")
def about():
	return render_template( "about.html" )

@app.route("/make/")
def get_makes():
	return models.get_makes().to_json()

@app.route("/model/<oid>")
def model_details(oid):
	m = models.get_model(oid)
	# print m.full_name
	return render_template('model-details.html', model=m)

@app.route("/model/<make_nice_name>/<model_nice_name>")
def get_model(make_nice_name, model_nice_name):
	m = models.get_model(make_nice_name, model_nice_name)
	# print m.full_name
	return render_template('model-details.html', model=m)

@app.route("/api/model/<make_name>/<model_name>")
def api_get_model(make_name, model_name):
	m = models.get_model(make_name, model_name)
	# print m.full_name
	return m.to_json()

@app.route("/variant/<model_id>/")
def model_variants(model_id):
	v = models.get_variants(model_id)
	return v.to_json()

@app.route("/model/top-weekly/")
def top_weekly():
	return render_template('model-details.html')

@app.route("/model/top-overall/")
def top_overall():
	return render_template('model-details.html')


# Run app
if __name__ == "__main__":
	app.debug = True
	app.run()