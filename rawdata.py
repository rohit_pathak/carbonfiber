from pymongo import MongoClient
from pprint import pprint
from models import *
from mongoengine import *
import collections
import re
import traceback
import requests
import time
import json

CACHE_DIR = 'data-cache/'

def cache_edmunds():
	WRITE_DIR = CACHE_DIR + 'edmunds/'
	KEY = 'nvk795fzfqtmjwuc7t6tum2k'
	KEY_SECRET = '4bgzRVEgbYZSEpbBYz9k9PFm'
	PHOTOS_BASE_URL = 'http://media.ed.edmunds-media.com'

	def largest_image(arr):
		# pprint(arr) 
		def func(o):
			return int(o['size'])
		images = [ {'size': re.findall(r'\d+', i.split('/')[-1].split('_')[-1])[0], 'url':i } for i in arr ]
		return max(images, key=func)['url']

	# Get makes. Returns makes and array of models for each make.
	data = json.loads( requests.get('https://api.edmunds.com/api/vehicle/v2/makes?state=new&view=basic&fmt=json&api_key=' + KEY).text )
	pprint( [ str(i) + ' - ' + a['name'] for i,a in enumerate(data['makes']) ])

	for m in data['makes'][28:29]: #[1:2], 
		print m['name']
		for mo in m['models']:
			for y in mo['years']:
				model_key = str(y['year']) + '_model'
				mo[ model_key ] = {'variants':[], 'photos':[]}
				model_images = []

				# Get styles for each model
				variant_data = json.loads( requests.get('https://api.edmunds.com/api/vehicle/v2/' + m['niceName'] + '/'+ mo['niceName'] +'/' + str(y['year']) + '/styles?state=new&view=full&fmt=json&api_key=' + KEY).text )
				print "Got variants for", mo['name'], model_key
				variants = []

				for v in variant_data['styles']: # save variant data for this model
					photos = json.loads( requests.get('https://api.edmunds.com/v1/api/vehiclephoto/service/findphotosbystyleid?styleId=' + str(v['id']) + '&fmt=json&api_key=' + KEY).text )
					if len(photos):
						try:
							photos = [ PHOTOS_BASE_URL + largest_image(o['photoSrcs']) for o in photos if len(o['photoSrcs']) ]
						except:
							print "protos error"
							pprint(photos)
							photos = []

					model_images += photos
					v.pop
					variants.append(v)

				mo[model_key]['variants'] = variants
				model_images.reverse()
				mo[model_key]['photos'] = list(set(model_images))
				
				time.sleep(2)

		wf = open( WRITE_DIR + m['name'] + '.json', 'w')
		wf.write( json.dumps(m) )
		wf.close()

		print "Inserted", m['name']
		print '--------------------'


import sys
if __name__ == '__main__':
	site = sys.argv[1]
	if site == 'edmunds':
		cache_edmunds()
	else:
		print "No such site can be cached yet."


