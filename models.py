from mongoengine import *
from pymongo import MongoClient
from bson.objectid import ObjectId
import re

client = MongoClient()
db = client.carbonfiber # to query through normal mymongo
connect('carbonfiber') # to query through mongoengine, if required

class User(Document):
	name = StringField(required=True)
	email = EmailField(required=True)
	password = StringField(required=True)


class Make(Document):
	e_id = IntField()
	name = StringField()
	name_lower = StringField()
	nice_name = StringField()
	description = StringField()


class SpecificationsRange(EmbeddedDocument):
	power = ListField( IntField() ) # [min, max]
	torque = ListField( IntField() ) # [min, max]
	mileage = ListField( IntField() )
	acceleration = ListField( IntField() ) # 0 - 100 km/h 
	top_speed = ListField( IntField() )
	fuel_types = ListField( StringField() )
	transmission_types = ListField( StringField() )
	body_types = ListField( StringField() )


class Specifications(EmbeddedDocument):
	body_type = StringField() # sedan, hatch etc.
	# Engine
	cylinders = IntField(default=0)
	configuration = StringField(default='')
	size = DecimalField(default=0)
	engine_type = StringField(default='')
	torque = IntField(default=0) # in Nm
	# torque_rpm = IntField(default=0)
	power = IntField(default=0) # in bhp
	# power_rpm = IntField(default=0)
	fuel_type = StringField(default='')
	# Transmission
	drive_type = StringField(default='')
	num_gears = StringField(default='')
	gearbox = StringField(default='') # e.g. 6 Speed Automatic
	# Fuel Economy
	mileage = DecimalField(default=0.0) # in kmpl
	mileage_city = DecimalField(default=0.0)
	mileage_highway = DecimalField(default=0.0)
	#Performance
	# acceleration = IntField(default=0) # 0-100 km/h in seconds
	# top_speed = IntField(default=0) # in km/h
	# Interiors
	doors = IntField(default=0)
	# seating_capacity = IntField(default=0)
	# boot_space = IntField(default=0) # litres
	# fuel_tank_capacity = IntField(default=0)
	# power_steering = BooleanField(default=False)
	# power_windows = StringField(default='')
	# airbags = BooleanField(default=False)
	# seat_upholstery = StringField(default='')
	# built_in_audio = BooleanField(default=False)
	# sunroof = BooleanField(default=False)
	# Dimensions
	# body_length = IntField(default=0) # in milimeters
	# body_width = IntField(default=0)
	# body_height = IntField(default=0)
	# ground_clearance = IntField(default=0)
	# weight = IntField(default=0) # in kilograms
	# wheelbase = IntField(default=0)
	# Other
	# traction_control = BooleanField(default=False)
	# turning_radius = IntField(default=0) # in meters
	# rear_brake = StringField(default='')
	# front_brake = StringField(default='')
	# tyre_size = StringField(default='')
	# rear_suspension = StringField(default='')
	# front_suspension = StringField(default='')
	# alloy_wheels = BooleanField(default=False)


class Model(Document):
	e_id = StringField()
	name = StringField()
	nice_name = StringField()
	full_name = StringField() # Make + Name
	name_lower = StringField() # full name in lowercase

	make = ReferenceField( Make, reverse_delete_rule=CASCADE )
	make_nice_name = StringField()
	years = ListField( IntField() ) # [2013, 2014, ...]
	
	price_range = ListField( IntField() ) # [min, max]
	specs_range = EmbeddedDocumentField( SpecificationsRange )
	images = ListField( StringField() ) # urls for images
	tags = ListField( StringField(max_length=30) )
	total_clicks = IntField(default=0)
	weekly_clicks = IntField(default=0)


class Variant(Document):
	e_id = StringField()
	name = StringField()
	name_lower = StringField()
	model = ReferenceField( Model, reverse_delete_rule=CASCADE )
	year = IntField()
	price = DecimalField()
	specs = EmbeddedDocumentField( Specifications )


####### Model Methods #######
def get_makes():
	return Make.objects()

def get_model(make_nice_name, nice_name):
	# print make_nice_name, nice_name
	return Model.objects.get(make_nice_name=make_nice_name, nice_name=nice_name)

def get_model_by_id(oid):
	return Model.objects.get(id=oid)

def get_models( keywords, budget, makes, mileage, body_types, page ):
	# print keywords
	QUERY_SIZE = 50
	min_budget, max_budget = budget
	query = { '$and' : [
						{ 'price_range.1' : {'$gte': min_budget } },
						{ 'price_range.0' : {'$lte': max_budget } },
					]}

	if len(keywords):
		keys = [ re.compile(k, re.IGNORECASE) for k in keywords ]
		query['$and'].append({ '$and': [ { 'name_lower': {'$in': [k] } } for k in keys ] })

	if len(makes):
		query['$and'].append({ '$or' : [ {'make' : ObjectId(m['_id']['$oid'])} for m in makes ] })

	if len(body_types):
		query['$and'].append({ '$or' : [ {'specs_range.body_types' : b} for b in body_types ] })

	if mileage > 1:
		query['$and'].append( { 'specs_range.mileage' : {'$gte' : mileage } } )

	models = db.model.find( query ).skip( page * QUERY_SIZE ).limit( QUERY_SIZE )
	return models


def get_models_by_params(params):
	max_budget = int(params['budget']['max']) #* 100000
	min_budget = int(params['budget']['min']) #* 100000
	mileage = float(params['mileage'])
	if max_budget == None: max_budget == 10000000
	query = { '$and' : [ 
						{ 'price_range.1' : {'$gte': min_budget } },
						{ 'price_range.0' : {'$lte': max_budget } },
					]}
	if len(params['makes']):
		query['$and'].append({ '$or' : [ {'make' : ObjectId(m['_id']['$oid'])} for m in params['makes'] ] })

	if mileage > 1:
		query['$and'].append( {'specs_range.mileage' : {'$gte' : mileage } } )

	models = db.model.find( query ).limit(70)
	return models

def get_models_by_keywords(keywords):
	keys = [ re.compile(k, re.IGNORECASE) for k in keywords ]
	data = db.model.find( { '$and': [ { 'name_lower': {'$in': [k] } } for k in keys ] }).limit(70)
	return data

def get_variants(model_id):
	m = Model.objects.get(id=model_id)
	v = Variant.objects(model=m)
	return v


