from pymongo import MongoClient
from pprint import pprint
from models import *
from mongoengine import *
import collections
import re
import traceback
import requests
import time
import json
import os


client = MongoClient()

def sync_with_autoevolution():
	
	cdb = client.cardb
	spec_categories = set([u'Brakes Specs',
			     u'Dimensions Specs',
			     u'Fuel Consumption Specs',
			     u'General Specs',
			     u'Notice',
			     u'Performance Specs',
			     u'Tires Specs',
			     u'Transmission Specs',
			     u'Weight Specs'])

	specs = set()

	for make in cdb.makes.find():
		make_name = make['name'].replace('-', ' ')
		# print make_name
		for model in make['models']:
			model_name = model['name'].replace( make_name.upper(), '').strip()
			# print model_name
			for v in model['variants']:
				variant_name = v['name'].replace( make_name.upper(), '').replace(model_name, '').strip()
				year = int( v['year'])
				# print '  ', year, variant_name
				# pprint.pprint(v)
				for c in spec_categories:
					if c in v:
						for s in v[c]:
							specs.add(s)

		# print
		# print '.....................'
		# print
	pprint.pprint(specs)


def sync_with_carwale():

	def clean_specs(specs):
		specs['Engine'] = specs.pop('Engine & Transmission')
		specs['Dimensions'] = specs.pop('Dimensions & Weight')
		specs['Seating'] = specs.pop('Capacity')
		specs['Other Specifications'] = specs.pop('Suspensions, Brakes, Steering & Tyres')
		
		specs['Engine']['Power'] = specs['Engine'].pop('Max Power')
		if 'rpm' in specs['Engine']['Power']:
			bhp, rpm = specs['Engine']['Power'].split('@')
			power = { 'bhp' : float( bhp.split()[0] ), 'rpm' : int( rpm.split()[0] ) }
		else:
			bhp, rpm = specs['Engine']['Power'], 0
			power = { 'bhp' : float( bhp.split()[0] ), 'rpm' : rpm }
		
		specs['Engine']['Power'] = power
		
		specs['Engine']['Torque'] = specs['Engine'].pop('Max Torque')
		nm, rpm = specs['Engine']['Torque'].split('@')
		torque = { 'nm' : float( nm.split()[0] ), 'rpm' : int( rpm.split()[0] ) }
		specs['Engine']['Torque'] = torque
		return specs

	cdb = client.carwale
	for m in cdb.makes.find():
		make = { 'name' : m['name'] }
		make_id = db.makes.insert( make )
		for mo in m['models']:
			model = { 	'name' : mo['name'],
						'make_id' : make_id,
						'specifications' : '',
						'price_range' : [] }
			model_id = db.models.insert( model )
			variants = []
			prices = []

			for v in mo['variants']:
				specs = v['specifications']
				
				try:
					specs = clean_specs(specs)
				except:
					pprint(v)

				price = v['price']
				
				if 'lakh' in price:
					price = float(price.split()[0]) * 100000
				elif 'crore' in price:
					price = float(price.split()[0]) * 10000000
				else:
					print 'price', price
				prices.append(price)

				variant = {
					'name' : v['name'],
					'model_id' : model_id,
					'specifications' : specs,
					'price' : price
				}
				variant_id = db.variants.insert(variant)
				variants.append(variant_id)

			model = db.models.update( {'_id' : model_id},
					{
				      '$set': {
				        'price_range' : [min(prices), max(prices)],
				        'specifications' : db.variants.find_one({'_id' : variants[ prices.index(max(prices)) ]})['specifications']
				      }
				    }
				)


def sync_with_zigwheels():
	
	client.drop_database('autobase_zig') # clear everything
	cdb = client.zigwheels
	connect('autobase_zig')

	def convert(data):
	    if isinstance(data, basestring):
	        return str(data)
	    elif isinstance(data, collections.Mapping):
	        return dict(map(convert, data.iteritems()))
	    elif isinstance(data, collections.Iterable):
	        return type(data)(map(convert, data))
	    else:
	        return data

	def mk_int(s):
		if len(s):
			if s[0] == '': return 0
			else: return int(s[0])
		else:
			return 0

	def mk_float(s):
		if len(s):
			if s[0] == '': return 0.
			else: return float(s[0])
		else: return 0.

	def mk_bool(s):
		if s == '': return None
		elif s == 'Y' or s == 'Alloy': return True
		else: return False

	for m in cdb.makes.find({ 'name': { '$ne': 'test' } }):
		make = Make(name = m['name'],
					name_lower = m['name'].lower(),
					logo_url = m['name'].replace(" ", "-") ).save()

		for mo in m['models']:
			model = Model(	name=mo['name'],
							name_lower=mo['name'].lower(),
							make=make,
							year=2014,
							in_production=True ).save()

			spec_range = SpecificationsRange()
			variants = []
			for var in mo['variants']:
				v = convert(var)
				price = mk_float(re.findall("\d+.\d+", v['price']))
				if 'lakh' in v['price']: price *= 100000
				elif 'crore' in v['price']: price *= 10000000

				variant = Variant(	name=v['name'],
									name_lower=v['name'].lower(),
									model=model,
									price=price ).save()
				variants.append(variant)

				try:
					v = v['specifications']
					s = Specifications()
					
					if len(v.keys()):
						print v['Boot Space']						
						# Engine
						s.cylinders = v['No Of Cylinders']
						s.displacement = mk_int(re.findall(r'\d+',v['Engine Displacement']))
						s.torque = mk_int(re.findall(r'\d+',v['Torque'])) # in Nm
						s.torque_rpm = 1500 # fixed, for now
						s.power = mk_int( [0.98592325737 * mk_int(re.findall(r'\d+',v['Power']))] ) # in hp
						s.power_rpm = 1500
						s.fuel_type = v['Fuel Type']
						# Transmission
						s.drive_type = v['Drive Type']
						s.gearbox = v['No Of Gears'] + ' speed ' + v['Transmission'] # e.g. 6 Speed Automatic
						# Fuel Economy
						s.mileage = mk_float(re.findall("\d+.\d+",v['Overall (Km/l)'])) # in kmpl
						#Performance
						s.top_speed = mk_int(re.findall(r'\d+', v['Top Speed (Km/h)'])) # in km/h
						# Interiors
						s.doors = mk_int(re.findall(r'\d+', v['Doors']))
						s.seating_capacity = mk_int( re.findall(r'\d+', v['Seating Capacity']) )
						s.boot_space = mk_int(re.findall(r'\d+',v['Boot Space'])) # litres
						s.fuel_tank_capacity = mk_int(re.findall(r'\d+',v['Fuel Capacity']))
						s.power_steering = mk_bool(v['Power Steering'])
						s.power_windows = v['Power Windows ( Front / Rear )']
						s.airbags = mk_bool(v['Airbags'])
						s.seat_upholstery = v['Seat Upholstery ( Leather/ Fabric)']
						s.built_in_audio = mk_bool(v['Music System'])
						s.sunroof = mk_bool(v['Sunroof / Moonroof'])
						# Dimensions
						dimensions = re.findall(r'\d+',v['Length Width Height'])
						if len(dimensions):
							s.length =  mk_int([dimensions[0]]) # in milimeters
							s.width = mk_int([dimensions[1]])
							s.height = mk_int([dimensions[2]])
						s.ground_clearance = mk_int(re.findall(r'\d+',v['Ground Clearance']))
						s.weight = mk_int(re.findall(r'\d+',v['Kerb Weight'])) # in kilograms
						s.wheelbase = mk_int( re.findall(r'\d+',v['Wheelbase']) )
						# Other
						s.traction_control = mk_bool(v['Traction Control System (TCS)'])
						s.turning_radius = mk_int( re.findall(r'\d+',v['Minimum Turning Radius'] ) ) # in meters
						s.rear_brake = v['Brakes Rear']
						s.front_brake = v['Brakes Front']
						s.tire_size = v['Tyre Size']
						s.rear_suspension = v['Suspension Rear']
						s.front_suspension = v['Suspension Front']
						s.alloy_wheels = mk_bool(v['Wheels Type (Pressed Steel/ Alloy)'])

					variant.specs = s
					variant.save()
					
				except:
					print "Error!"
					print model.name, variant.name
					pprint(v)
					traceback.print_exc()
					return
			print model.name, [v.name for v in variants]
			if len(variants):
				print [v.name for v in variants]
				model.price_range = [ min([v.price for v in variants]), max([v.price for v in variants]) ]
				s_range = SpecificationsRange()
				s_range.power = [ min([v.specs.power for v in variants]), max([v.specs.power for v in variants]) ]
				s_range.torque = [ min([v.specs.torque for v in variants]), max([v.specs.torque for v in variants]) ]
				s_range.mileage = [ min([v.specs.mileage for v in variants]), max([v.specs.mileage for v in variants]) ]
				s_range.top_speed = [ min([v.specs.top_speed for v in variants]), max([v.specs.top_speed for v in variants]) ]
				s_range.fuel_types = list( set( [v.specs.fuel_type for v in variants] ) )

				model.specs_range = s_range
				print s_range.power
				print s_range.torque
				print s_range.mileage
				print s_range.top_speed
				print s_range.fuel_types	
			model.save()


def sync_with_edmunds(remote=False):
	DATA_DIR = 'data-cache/edmunds/'
	datafiles = os.listdir(DATA_DIR)

	if not remote:
		client.drop_database('carbonfiber') # clear everything
		connect('carbonfiber')
	else:
		connect('carbonfiber', username='admin', password='dmhX67dRMnFU',  host='127.0.0.1', port=46636)
		print 'connected to remote... '

	def get_body_type(b):
		HATCHBACK = 'hatchback'
		SEDAN = 'sedan'
		CONVERTIBLE = 'convertible'
		VAN = 'van'
		SUV = 'suv'
		TRUCK = 'truck'
		m = {'Wagon':HATCHBACK, '4dr SUV':SUV, '4dr Hatchback':HATCHBACK,
			'2dr Hatchback':HATCHBACK, 'Passenger Van':VAN, 'Cargo Minivan':VAN,
			'Coupe':SEDAN, 'Sedan':SEDAN, 'Convertible':CONVERTIBLE,
			'Crew Cab Pickup':TRUCK, 'Extended Cab Pickup':TRUCK,
			'Cargo Van':VAN, 'Regular Cab Pickup':TRUCK, 'Passenger Minivan':VAN}
		# To add: Crew Cab Pickup, Extended Cab Pickup, Passenger Van, Cargo Van, Regular Cab Pickup,
		# Passenger Minivan,
		if b in m:
			return m[b]
		else:
			print b
			return 'other'

	def get_specifications(data):
		s = Specifications()
		s.body_type = get_body_type( data['categories']['vehicleStyle'] )
		s.doors = data.get('numOfDoors',0)
		# Transmission
		s.drive_type = data['drivenWheels']
		s.num_gears = str(data['transmission']['numberOfSpeeds'])
		s.gearbox = data['transmission']['transmissionType'].replace('_', ' ').lower()
		# Fuel Economy
		if 'MPG' in data:
			# print 'yay! mpg'
			s.mileage_city = float(data['MPG']['city'])
			s.mileage_highway = float(data['MPG']['highway'])
			s.mileage = 0.45*s.mileage_highway + 0.55*s.mileage_city
		# Engine
		try:
			s.cylinders = data['engine'].get('cylinder',0)
			s.configuration = data['engine'].get('configuration', '')
			s.engine_type = data['engine']['name']
			s.power = data['engine'].get('horsepower',0)
			s.fuel_type = data['engine']['fuelType'].replace('(required)', '').strip()
			s.torque = data['engine'].get('torque', 0)
			s.size = data['engine'].get('size',0)
		except:
			traceback.print_exc()
			pprint(data['engine'])
		return s

	# Get makes. Returns makes and array of models for each make.
	b_types = set()
	for fname in datafiles:
		f = open( DATA_DIR + fname, 'r' )
		m = json.loads( f.read() )
		f.close()
		make = Make(name = m['name'], 
					name_lower=m['name'].lower(),
					nice_name=m['niceName'],
					e_id=m['id'],
					description='').save()
		print make.name

		for mo in m['models']:
			model = Model( 	e_id = str(mo['id']),
							name = mo['name'],
							full_name = make.name + ' ' + mo['name'],
							name_lower = (make.name +' '+ mo['name']).lower(),
							nice_name = mo['niceName'],
							make = make,
							make_nice_name= make.nice_name).save()
			years = []

			# For each year get the variants
			variants = []
			for y in reversed(mo['years']):
				years.append( y['year'] )
				model_images = mo[ str(y['year']) + '_model']['photos']
				if type(model_images) == list and len(model_images):
					model.images = model_images
				else:
					model.images = ['/static/img/hatchback-icon.png']

				# Get styles for each model
				variant_data = mo[ str(y['year']) + '_model']['variants']
				for v in variant_data: # save variant data for this model
					print "Inserting " + model.name + ' ' + str(y['year']) + ' ' + v['name']
					variant = Variant(	name=v['name'],
										name_lower=(model.name + ' ' + v['name']).lower(),
										e_id=str(v['id']),
										model=model,
										year=y['year'],
										price=v['price']['baseMSRP'] )

					variant.specs = get_specifications(v)
					variant.save()
					variants.append(variant)
					b_types.add(variant.specs.body_type)

			# update model according to variant data
			model.years = sorted(years)
			model.save()
			if len(variants): # calulate ranges for this model
				model.price_range = [ min([v.price for v in variants]), max([v.price for v in variants]) ]
				s_range = SpecificationsRange()
				s_range.power = [ min([v.specs.power for v in variants]), max([v.specs.power for v in variants]) ]
				s_range.torque = [ min([v.specs.torque for v in variants]), max([v.specs.torque for v in variants]) ]
				s_range.mileage = [ min([v.specs.mileage for v in variants]), max([v.specs.mileage for v in variants]) ]
				s_range.fuel_types = list( set( [v.specs.fuel_type for v in variants] ) )
				s_range.body_types = list( set( [v.specs.body_type for v in variants] ) )
				s_range.transmission_types = list( set( [v.specs.gearbox for v in variants] ) )
				model.specs_range = s_range
				model.save()

	pprint(b_types)

import sys
if __name__ == '__main__':
	site = sys.argv[1]
	if site == 'edmunds':
		sync_with_edmunds(remote=False)
	else:
		print "No data/configuration available for", site




