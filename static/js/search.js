var app = angular.module('searchApp', ['ui.slider', 'ui.bootstrap', 'infinite-scroll']);

/* Controllers */
app.controller('SearchController', ['$http', function($http){
	var searchCtrl = this;
	this.cars = [];
	this.pinned = [];
	this.searchBusy = true;
	this.searchCount = 0;
	this.searchViewCondensed = false;

	this.params = {
		keywords : '',
		budget : { min:'10000', max:'200000' },
		makes : [],
		mileage : 1,
		bodyTypes : { hatchback:false, sedan:false, convertible:false, suv:false },
		nextPage : 0
	};

	this.math = window.Math;

	$http.get('/make/').success( function( data, status ){
		for (var i=0; i < data.length; i++){
			data[i].is_selected = false;
		}
		searchCtrl.params.makes = data;
	});

	this.searchCars = function(page){
		searchCtrl.params.nextPage = page | 0;
		if (searchCtrl.params.nextPage === 0){
			searchCtrl.cars = [];
		}

		searchCtrl.searchBusy = true;
		$http.post('/search/', {'params' : searchCtrl.params}).success(function(data, status){
			data.cars.forEach(function(car){ car.condenseView = searchCtrl.searchViewCondensed;});
			searchCtrl.cars = searchCtrl.cars.concat( data.cars );
			searchCtrl.params.nextPage = data.nextPage;
			searchCtrl.searchCount = data.searchCount;
			searchCtrl.searchBusy = false;
		});
	};

	this.loadMoreCars = function(){
		if (searchCtrl.params.nextPage >= 0 && !searchCtrl.searchBusy) {
			searchCtrl.searchCars(searchCtrl.params.nextPage);
		}
	};

	this.addBodyType = function(type){
		searchCtrl.params.bodyTypes[type] = ! searchCtrl.params.bodyTypes[type];
		searchCtrl.searchCars();
	}

	this.condenseSearchView = function(){
		searchCtrl.searchViewCondensed = true;
		searchCtrl.cars.forEach( function(car){
			car.condenseView = true;
		});
	};

	this.expandSearchView = function(){
		searchCtrl.searchViewCondensed = false;
		searchCtrl.cars.forEach( function(car){
			car.condenseView = false;
		});
	};
	
	this.pinCar = function(car){
		var alreadyPinned = false;
		for (var i = 0; i<searchCtrl.pinned.length; i++){
			if (carEquals(car, searchCtrl.pinned[i])) {
				alreadyPinned = true;
			}
		}
		if (!alreadyPinned){
			searchCtrl.pinned.push(car);
		}
		
		// this.cars.splice( this.cars.indexOf(car), 1 );
	};

	this.unpinCar = function(car){
		// searchCtrl.cars.push(car);
		searchCtrl.pinned.splice( searchCtrl.pinned.indexOf(car), 1 );	
	};

	this.showVerticalRule = function(e){
		$('.vertical-rule').removeClass('hide');
		var parentOffset = $('.vertical-rule').parent().offset();
		var x = e.pageX - parentOffset.left - 3;
		var y = e.pageY;
		$('.vertical-rule').attr("style", "left:" + x + "px;");
	};

	this.hideVerticalRule = function(){
		$('.vertical-rule').addClass('hide');
	};

	this.showTooltip = function(c, values){
		var options = {
			'show': true,
		    'placement': 'top',
		    'title': values + ' hp'
		};
		$('.'+ c).tooltip(options)
	};

	this.getInitials = function(str){
		var arr = str.split(" ");
		var s = '';
		for (var i = 0; i < arr.length; i++){
			if (arr[i].charAt(0) == '('){
				s += ' (' + arr[i].charAt(1) + ') '
			} else {
				s += arr[i].charAt(0);
			}
		}
		return s;
	};
}]);

function carEquals(car1, car2){
	return car1.e_id === car2.e_id;
}

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
});
