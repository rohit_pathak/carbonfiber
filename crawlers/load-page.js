var page = require('webpage').create();
var fs = require('fs');// File System Module
var system = require('system');

var args = system.args;
var output = './temp-htmls/temp.html'; // path for saving the local file 
var page_url = system.args[1];
page.open(page_url, function() { // open the file 
  fs.write(output, page.content,'w'); // Write the page to the local file using page.content
  phantom.exit(); // exit PhantomJs
});