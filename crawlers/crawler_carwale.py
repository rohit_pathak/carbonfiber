import requests
from pymongo import MongoClient
from pprint import pprint
from bs4 import BeautifulSoup
import time
import random

# Mongodb initialization
client = MongoClient()
db = client.carwale

start_url = 'http://www.carwale.com'

page = requests.get('http://www.carwale.com/new/')
page_makes = BeautifulSoup(page.text)

makes = []
for m in page_makes.select( 'div.brands-list li' ):
	# print m.find('a')['href']
	makes.append( { 'url' : start_url + m.find('a')['href'],
		 			'name': m.find('a')['title'] } )


for make in makes[5:]:
	
	make_obj = { 'name': make['name']}
	# Get all models of that brand
	page = requests.get(make['url'])
	page_models = BeautifulSoup(page.text)
	models = []
	for m in page_models.select('#divModels td a.href-title'):
		models.append({
			'name' : m['title'],
			'url' : start_url + m['href']
			})

	make_obj['models'] = []
	
	for model in models:
		print model['name']
		model_obj = { 'name' : model['name'] }
		#Get all variants of that model
		page = requests.get(model['url'])
		page_variants = BeautifulSoup( page.text )

		model_obj['variants'] = []

		for v in page_variants.select('#tblVersions td a.href-title'):
			variant_obj = {'name' : v['title'].replace(make['name'], '').replace(model['name'], '').strip()}
			specs_url = start_url + v['href'].replace('details', 'specifications')
			variant_obj['url'] = specs_url
			
			page = requests.get(specs_url)
			page_specs = BeautifulSoup(page.text)
			
			price = page_specs.select('#divPricingNew .fontblack')[0].get_text().replace('Price :', '').strip().split(' ', 1)[1]
			print '  ' + variant_obj['name'], price
			
			variant_obj['price'] = price
			specs = {}

			for table in page_specs.select('.tab-nav-panel table'):
				category = table.find('th').get_text().strip()
				data = []
				for d in table.select('td'):
					data.append(d.get_text().strip())

				specs[category] = {}
				for i in range( len(data) ):
					if i%2 == 0 and i+1 < len(data):
						specs[category][data[i]] = data[i+1]

			variant_obj['specifications'] = specs
			model_obj['variants'].append(variant_obj)
		
		time.sleep(4)
		make_obj['models'].append(model_obj)
		

	db.makes.insert(make_obj)
	print 'inserted', make_obj['name']
	print '________________ \n'

