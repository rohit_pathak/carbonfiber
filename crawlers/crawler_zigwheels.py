import requests
from pymongo import MongoClient
from pprint import pprint
from bs4 import BeautifulSoup
import time
import random
import os
from subprocess import call, STDOUT

# Mongodb initialization
client = MongoClient()
db = client.zigwheels

FNULL = open(os.devnull, 'w')

start_url = 'http://www.zigwheels.com'

page = requests.get('http://www.zigwheels.com/buy-sell-car')
page_makes = BeautifulSoup(page.text)

makes = []
for m in page_makes.select( 'div.brand_logo li' ):
	# print m.find('a')['href']
	makes.append( { 'url' : m.find('a')['href'],
		 			'name': m.find('p').get_text() } )

# pprint(makes)

for make in makes[17:]:
	
	make_obj = { 'name': make['name']}
	# Get all models of that brand
	page = requests.get(make['url'])
	page_models = BeautifulSoup(page.text)
	models = []
	for m in page_models.select('#allModelVariants li .row .tabRightContent h3 a'):
		models.append({
			'name' : m.find('span').get_text(),
			'url' : m['href']
			})

	# pprint(models)

	make_obj['models'] = []
	
	for model in models:
		print model['name']
		model_obj = { 'name' : model['name'] }
		
		#Get all variants of that model
		c = call( [ "phantomjs", "load-page.js", model['url'] ], stdout=FNULL, stderr=STDOUT )
		page = open("temp-htmls/temp.html", "r")
		page_variants = BeautifulSoup( page.read() )
		page.close()
		
		model_obj['variants'] = []
		print "I see", len(page_variants.select('#variantDetails tr')) - 1, "variants"

		for v in page_variants.select('#variantDetails td.ro-table-blue-bigtxt a'):

			variant_obj = {'name' : v.get_text().replace(make['name'], '').replace(model['name'], '').replace(u'\xa0', u' ').strip()}
			specs_url = v['href']
			variant_obj['url'] = specs_url
			
			c = call( [ "phantomjs", "load-page.js", specs_url ], stdout=FNULL, stderr=STDOUT )
			page = open("temp-htmls/temp.html", "r")
			page_specs = BeautifulSoup( page.read() )
			page.close()
			
			price = page_specs.select('.newCarPriceText div')[0].get_text() \
						.replace('(Ex-showroom Price)', "") \
						.replace('Rs.', "")
			price =  " ".join(price.split())
			print '  ' + variant_obj['name'], price
			
			variant_obj['price'] = price
			specs = {}

			for row in page_specs.select('#accordion .panel .panel-body .row'):
				key = row.select('div')[0].get_text().strip() 
				val = row.select('div')[1].get_text().strip()
				specs[key] = val

			variant_obj['specifications'] = specs
			model_obj['variants'].append(variant_obj)
		
		time.sleep(4)
		make_obj['models'].append(model_obj)
		

	db.makes.insert(make_obj)
	print 'inserted', make_obj['name']
	print '________________ \n'
	# pprint(make_obj)

