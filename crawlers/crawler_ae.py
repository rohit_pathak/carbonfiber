import requests
from pymongo import MongoClient
import pprint
from bs4 import BeautifulSoup
import time
import random

# Mongodb initialization
client = MongoClient()
db = client.cardb


###### CRAWL! ######
page = requests.get('http://www.autoevolution.com/cars/')
page_makes = BeautifulSoup(page.text)

make_urls = []
for l in page_makes.select( 'div.brandlist div a' ):
	make_urls.append( l['href'] )

for make_url in [make_urls[3]]: # Left off at Subaru
	make = {} # The collection to be inserted in the db
	make_name = make_url[ make_url.index('.com/') + 5: -1]
	make['name'] = make_name
	print "Getting data for " + make_name
	
	# Get models currently in production of that make
	make['models'] = []
	page = requests.get( make_url ) # now at the models page
	page_models = BeautifulSoup(page.text)
	print "Models in Production ///"
	print

	for m in page_models.find('div', class_='carslist').find_all('div', class_='carslist-item'): # for each model
		model = {}
		model_name = m.select('h2 a')[0]['title']
		print model_name
		model['name'] = model_name

		
		# Get all variants of that model
		model['variants'] = []
		page = requests.get( m.select('h2 a')[0]['href'] ) # page for that model
		page_variants = BeautifulSoup(page.text)

		for series in page_variants.select('div.seriesbox .seriesboxrow'):
			year = series.select('div.fl b')[0].get_text()
			print year
			specs_page_url = series.select('div.fr h2 a')[0]['href']
			specs_page = BeautifulSoup( requests.get( specs_page_url ).text )
			
			for v in specs_page.select('div.engine-block'):
				variant = {}
				variant['name'] = v.select('h3 span')[0].get_text()
				variant['year'] = year
				variant['url'] = specs_page_url
				print variant['name']
				
				# Get specifications for that variant
				for category in v.select('.enginedata dl'):
					spec_name = category['title']
					variant[spec_name] = {}
					for spec, n in zip(category.select('dt'), category.select('dd')):
						variant[spec_name][spec.get_text()] = str(n)[4:-5].strip().split('<br/>')
				
				model['variants'].append(variant)
			
			time.sleep(2)

		make['models'].append(model)
	
	# # Get out of production models.
	# print 'Models out of production ///'
	# for car in page_models.select('div.carslist.faded .carslist-item'):
	# 	model = {}
	# 	model_name = m.select('h2 a')[0]['title']
	# 	print model_name
	# 	model['name'] = model_name

		
	# 	# Get all variants of that model
	# 	model['variants'] = []
	# 	page = requests.get( m.select('h2 a')[0]['href'] ) # page for that model
	# 	page_variants = BeautifulSoup(page.text)

	# 	for series in page_variants.select('div.seriesbox .seriesboxrow'):
	# 		year = series.select('div.fl b')[0].get_text()
	# 		print year
	# 		for engines in series.select('div.seriesboxengines'):
	# 			fuel_type = engines.select('h4')[0].get_text()
	# 			for v in engines.select('ul li a'):
	# 				print fuel_type, v['title']
	# 				# Get details for that variant
	# 				variant = {}
	# 				variant['name'] = v['title']
	# 				variant['fuel_type'] = fuel_type
	# 				variant['year'] = year
	# 				variant['url'] = v['href']
					
	# 				# Get specifications for that variant
	# 				variants_page = BeautifulSoup( requests.get( v['href'] ).text )
	# 				for category in variants_page.select('div#' + v['href'].split('#a',1)[1] + ' .enginedata dl'):
	# 					spec_name = category['title']
	# 					variant[spec_name] = {}
	# 					for spec, n in zip(category.select('dt'), category.select('dd')):
	# 						variant[spec_name][spec.get_text()] = str(n)[4:-5].split('<br/>')
					
	# 				model['variants'].append(variant)		
					
	# 	make['models'].append(model)

	# insert make to database
	db.makes.insert(make)
	print "inserted", make['name']
	time.sleep( random.randint(60,120) )


print		
print "Done Crawling. Bye :)"


